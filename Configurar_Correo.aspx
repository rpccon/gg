﻿<%@ Page Title="Configurar Correo" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Configurar_Correo.aspx.cs" Inherits="SICRER_Web_Application.Configurar_Correo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-3 space-up">
        <ul class="nav nav-pills nav-stacked well">
            <li><a href="ConfigurarAdministradores.aspx">Usuarios</a></li>
            <li><a href="Configurar_Semestres.aspx">Semestres</a></li>
            <li><a href="Indicadores.aspx">Indicadores</a></li>
            <li><a href="Carreras.aspx">Carreras</a></li>
            <li class="active"><a href="Configurar_Correo.aspx">Correo</a></li>
        </ul>
    </div>
    <div class="col-md-9 space-up">
        <div class="space-down">
            <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/img/ok.png"/>
                <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
                <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
                <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
                <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>
        </div>
         <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Aviso!</strong> El restablecimiento de la contraseña para los administradores solo es posible si existe
                una cuenta de correo, como usted es el super usuario, es necesario que brinde los siguientes datos. La contraseña
                de su correo estará encriptada en la base de datos.
          </div>
        <div class="form-horizontal space-up">
            <div class="form-group">
                <label class="col-md-4 control-label" for="textinput">Email</label>
                <div class="col-md-4">
                    <asp:TextBox ID="tbEmail" runat="server" CssClass="form-control  input-md" placeholder="Email"></asp:TextBox>
                </div>
            </div>
            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="passwordinput">Contraseña</label>
                <div class="col-md-4">
                    <asp:TextBox ID="tbContrasena1" TextMode="Password" runat="server" placeholder="Contrasena" CssClass="form-control input-md"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                        runat="server"
                        ErrorMessage="Digite Contraseña" Enabled="false" ControlToValidate="tbContrasena1"
                        Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="passwordinput">Repetir Contraseña</label>
                <div class="col-md-4">
                    <asp:TextBox ID="tbRepetirContrasena1" TextMode="Password" runat="server" placeholder="Repetir Contraseña" CssClass="form-control input-md"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                        runat="server"
                        ErrorMessage="Digite repetir contraseña" Enabled="false" ControlToValidate="tbRepetirContrasena1"
                        Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>
                <div class="col-md-4">
                     <asp:LinkButton ID="btnEnviarEmail" runat="server" OnClick="btnEnviarEmail_Click" CssClass="btn btn-success" Text="Actualizar Email"></asp:LinkButton>
                </div>
            </div>
        </div>
        <script src="Scripts/bootstrap.min.js"></script>
    </div>
</asp:Content>
