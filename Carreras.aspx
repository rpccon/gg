﻿<%@ Page Title="Carreras" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Carreras.aspx.cs" Inherits="SICRER_Web_Application.Carreras" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <link href="Content/jquery-ui-smoothness-1.11.4.min.css" rel="stylesheet" />
    <link href="Content/jquery-ui-smoothness-theme-1.11.4.css" rel="stylesheet" />
    <script src="Scripts/jquery-ui-1.11.4.js"></script>

    <style>
        .border-eliminar{
            border-radius: 30px;
        }
    </style>

    <script>
        $(document).ready(function () {

            $(function () {
                $("#MainContent_acordion").accordion({ active: false, collapsible: true });
            });
        })

        function ocultarElemento(ele) {
            $('#' + ele).hide();
        }

        function mostrarElemento(ele) {
            $('#' + ele).show();
        }
    </script>

    <div class="col-md-3 space-up">
        <ul class="nav nav-pills nav-stacked well">
            <li><a href="ConfigurarAdministradores.aspx">Usuarios</a></li>
            <li><a href="Configurar_Semestres.aspx">Semestres</a></li>
            <li><a href="Indicadores.aspx">Indicadores</a></li>
            <li class="active"><a href="Carreras.aspx">Carreras</a></li>
             <li id="confEmail" runat="server" visible="false"><a href="Configurar_Correo.aspx">Correo</a></li>
        </ul>
    </div>
    <div class="col-md-9 space-up">
        <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
            <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
            <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
            <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
            <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <div class="form-inline" style="margin-bottom:20px">
            <asp:LinkButton id="linkNuevaCar" runat="server" Text=" Crear Nueva " CssClass="btn btn-primary" OnClientClick="mostrarElemento('formRegistrarCar'); return false;"/>
        </div>
        <div id="formRegistrarCar" class="space-up" style="display:none;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Registrar Carrera</div>
                </div>
                <div class="panel-body form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="lbNombreCar" runat="server" Text="Nombre " CssClass="control-label col-sm-3" Font-Bold="true"></asp:Label>
                        <asp:TextBox ID="tbNombreCar" runat="server" CssClass="form-control col-sm-4"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8" style="align-content:center">
                            <asp:Button ID="btnRegistrarCar" runat="server" Text="Agregar" CssClass="btn btn-success" OnClick="btnRegistrarCar_Click"/>
                            <asp:Button ID="btnCancelarRegistroCar" runat="server" Text="Cancelar" CssClass="btn btn-primary" OnClientClick="ocultarElemento('formRegistrarCar'); return false;"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="acordion" runat="server"></div>
        <script src="Scripts/bootstrap.min.js"></script>
   </div>
</asp:Content>
