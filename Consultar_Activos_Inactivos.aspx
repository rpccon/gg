﻿<%@ Page Title="Consultar Activos/Inactivos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Consultar_Activos_Inactivos.aspx.cs" Inherits="SICRER_Web_Application.Consultar_Activos_Inactivos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-12">
        <div class="col-md-3 space-up">
            <ul class="nav nav-pills nav-stacked well">
                <li><a href="Consultar_Revisiones.aspx"> Consultar Revisiones</a></li>
                <li><a href="Configurar_Grupos_Admin.aspx"> Consultar Grupos</a></li>
                <li><a href="Consultar_Estudiantes.aspx">Consultar Estudiantes</a></li>
                <li class="active"><a href="Consultar_Activos_Inactivos.aspx">Consultar Activos/Inactivos</a></li>
            </ul>
        </div>

        <div class="col-md-9 space-up">

            <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
                <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
                <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
                <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
            </asp:Panel>

            <div class="form-inline">
                <asp:DropDownList id="ddTp_Estado" runat="server" CssClass="selectpicker form-control" Width="125px" AutoPostBack="true">
                    <asp:ListItem Text="Activos" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Inactivos" Value="0"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="space-down space-up">
                <asp:Label ID="lbResultados" runat="server" Font-Bold="true" Font-Size="Medium"></asp:Label>
            </div>
            <asp:GridView ID="gvEstudiantesAI" runat="server"  AutoGenerateColumns="False" CssClass="table table-bordered table-hover space-up">
                <Columns>
                    <asp:BoundField DataField="NOMBRE" HeaderText="Estudiante"/>
                    <asp:BoundField DataField="CARNE" HeaderText="Carné"/>
                </Columns>
            </asp:GridView>
        </div>
        <script src="Scripts/bootstrap.min.js"></script>
    </div>
</asp:Content>
