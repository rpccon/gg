﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Estadisticas.aspx.cs" MasterPageFile="~/Site.Master" Inherits="SICRER_Web_Application.Estadisticas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:HiddenField ID="HiddenField1" Value="1" runat="server" />


    <div class="col-md-3 space-up">
        <ul class="nav nav-pills nav-stacked well">
            <li class="active"><a href="Graficos/CumpIndicadores.aspx">Cumplimiento de indicadores</a></li>
            <li><a href="Graficos/IncumpIndicadores.aspx">Incumplimiento de indicadores</a></li>
            <li><a href="Graficos/AlasCumpResis.aspx">Cumplimiento de residencias por alas</a></li>
            <li><a href="Graficos/CuartosDB.aspx">Cuartos buenos y deficientes</a></li>
            <li><a href="Graficos/LlamadasAtencion.aspx">Registros llamadas de atención</a></li>
        </ul>
    </div>

    <div class="col-md-9 space-up">
        <div id="paneMensajes">
        <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/img/ok.png"/>
            <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
            <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
            <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>

        <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
            <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
            <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
        </asp:Panel>
            </div>
        <nav class="navbar navbar-default">
            <div class="container-fluid ">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Menu</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li id="tipoAdmin" onclick="selectorAdministrador(0); return false;" class="active" data-trigger="focus" data-toggle="tooltip" data-placement="bottom" title="Seleccionar"><a href="#">Administrador <span class="sr-only">(current)</span></a></li>
                        <li id="tipoRev" onclick="selectorRevisor(0); return false;" data-toggle="tooltip" data-placement="bottom" title="Seleccionar"><a href="#">Revisor</a></li>
                        <li id="tipoCor" onclick="selectorCoordinador(0); return false;" data-toggle="tooltip" data-placement="bottom" title="Seleccionar"><a href="#">Coordinador</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Registrar <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a onclick="mostrarFormulario(); selectorAdministrador(0); ">Administrador</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a onclick="mostrarFormulario(); selectorRevisor(0); return false;">Revisor</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a onclick="mostrarFormulario(); selectorCoordinador(0); return false;">Coordinador</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="form-inline">
                        <div class="form-group navbar-form navbar-left" role="search">
                            <!-- <input type="text" id="buscar" class="form-control" placeholder="Buscar Administrador">  -->
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server"
                                UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:TextBox ID="buscar" CssClass="form-control"  placeholder="Buscar Administrador" runat="server"></asp:TextBox>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="buscar" EventName="TextChanged" />

                                </Triggers>

                            </asp:UpdatePanel>

                        </div>
                        
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>


        <div id="formulario" style="display: none;" class="form-horizontal">
            <fieldset>
                <!-- Form Name -->

                <legend id="tipoUsuario">Registro de Administrador</legend>
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Nombre Completo</label>
                    <div class="col-md-4">

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="tbNombre1" runat="server" placeholder="Nombre Completo" CssClass="form-control input-md"></asp:TextBox>
                                <asp:RequiredFieldValidator Enabled="false" ID="RequiredFieldValidator1"
                                    runat="server"
                                    ErrorMessage="Digite nombre" ControlToValidate="tbNombre1"
                                    Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>


                <!-- Text input-->
                <div id="divCarne" style="display: none;" class="form-group">
                    <label class="col-md-4 control-label" id="lblCarne" for="textinput">Carne</label>
                    <div class="col-md-4">


                        <asp:UpdatePanel ID="UpdatePanel2" runat="server"
                            UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="tbCarne1" runat="server" AutoPostBack="true" TextMode="Number" CssClass="form-control input-md" placeholder="Carne"></asp:TextBox>
                                <asp:RequiredFieldValidator Enabled="false" ID="RequiredFieldValidator2"
                                    runat="server"
                                    ErrorMessage="Digite carne" ControlToValidate="tbCarne1"
                                    Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="tbCarne1" EventName="TextChanged" />
                            </Triggers>

                        </asp:UpdatePanel>
                    </div>
                </div>

                <div id="divEmail" style="display: block;">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Email</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="tbEmail1" TextMode="Email" runat="server" placeholder="Email" CssClass="form-control input-md"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                runat="server"
                                ErrorMessage="Digite email" Enabled="false" ControlToValidate="tbEmail1"
                                Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                        </div>
                    </div>
                </div>


                <!-- Text input-->
                <div id="divTelefono" class="form-group">
                    <label class="col-md-4 control-label" id="lblTelefono" for="textinput">Telefono</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="tbTelefono1" runat="server" placeholder="Teléfono" CssClass="form-control input-md"></asp:TextBox>

                    </div>
                </div>


                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Usuario</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="tbUsuario1" runat="server" placeholder="Usuario" CssClass="form-control input-md"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                            runat="server"
                            ErrorMessage="Digite usuario" Enabled="false" ControlToValidate="tbUsuario1"
                            Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                    </div>
                </div>


                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="passwordinput">Contraseña</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="tbContrasena1" TextMode="Password" runat="server" placeholder="Contrasena" CssClass="form-control input-md"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                            runat="server"
                            ErrorMessage="Contraseña debe tener entre 6 y 10 digitos válidos (números y letras)." Enabled="false" ControlToValidate="tbContrasena1"
                            Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="passwordinput">Repetir Contraseña</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="tbRepetirContrasena1" TextMode="Password" runat="server" placeholder="Repetir Contraseña" CssClass="form-control input-md"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                            runat="server"
                            ErrorMessage="Digite repetir contraseña" Enabled="false" ControlToValidate="tbRepetirContrasena1"
                            Display="Dynamic" SetFocusOnError="True" CssClass="alert-text" />
                    </div>
                </div>
                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="singlebutton"></label>
                    <div class="col-md-4">
                        <div class="col-md-6">
                            <%--OnClientClick="SetDataField(document.getElementById('<%=HiddenField1.ClientID %>').value);"--%>
                            

                        </div>
                        <div class="col-md-6">
                            <button id="singlebutton1" name="singlebutton" onclick="ocultarFormulario(); return false;" class="btn btn-default">Cancelar</button>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>





        <script src="Scripts/bootstrap.min.js"></script>
    </div>
</asp:Content>
