﻿<%@ Page Title="Configurar Grupos" Language="C#" MasterPageFile="~/Coordinador.Master" AutoEventWireup="true" CodeBehind="Configurar_Grupos.aspx.cs" Inherits="SICRER_Web_Application.Configurar_Grupos" EnableEventValidation="false"
    Culture="Auto" UICulture="Auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/Site.css" rel="stylesheet" />
    <script src="Scripts/site.js" type="text/javascript"></script>
    
    <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
     	<ContentTemplate>
            <div class="col-md-9 space-up">
                <div>
                    <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
                        <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
                        <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </asp:Panel>

                    <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
                        <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                        <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </asp:Panel>

                    <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
                        <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
                        <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </asp:Panel>    
                </div>
                <br />
        
                <asp:Label ID="Titulo1" Text="Recomendaciones" runat="server" 
                    Font-Size="Large" ForeColor="Black" Font-Bold="True" Font-Names="Arial">
                </asp:Label>
                <br /><br />
                <p>
                    Algunas recomendaciones antes de empezar a editar la tabla
                </p>
                <hr />
                <p>
                    1) Debes asignar los grupos de manera ordenada, es decir, empezar asignando estudiantes primero al grupo A
                    luego al B, y así sucesivamente hasta el grupo E. De esta manera se evita confusiones en el horario final.<br />
                    2) Intenta dejar los grupos de cocina lo más equilibrados posibles.<br />
                    3) Si tienes algún error asignando el grupo a un estudiante, es permitido que lo cambies.<br />
                    4) No dejes ningún estudiante sin grupo. Los estudiantes sin grupo se colorean de un tono anaranjado en su fila.<br />
                    5) Puedes revisar la tabla resumen ubicada al final de la página, para visualizar los datos de los grupos de una forma más elaborada.<br />
                    6) El horario no puede ser cambiado por el coordinador ni por nadie, ya que se asigna directamente al crearse el grupo.<br />
                    7) No olvides especificar si el horario es rotativo o fijo. Si es elegido rotativo significa que cada semana el día de limpieza
                    cambia para cada grupo (corriéndose un día), por el contrario si es fijo se mantendrá el mismo día todo el semestre para cada grupo.
                </p>
                <hr />


               <!-- <asp:Label ID="Titulo2" Visible="false" Text="Tipo de horario" runat="server" 
                    Font-Size="Large" ForeColor="Black" Font-Bold="True" Font-Names="Arial">
                </asp:Label>
                <br /><br />
                <p>
                    Elige un tipo de horario (por defecto siempre es rotativo)
                </p>
                <hr />
                <asp:RadioButtonList ID="RadioButtonList1" Visible="false" AutoPostBack="true" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                    <asp:ListItem>Rotativo</asp:ListItem>
                    <asp:ListItem>Fijo</asp:ListItem>
                </asp:RadioButtonList>-->
                <hr />        
                <br />
                <br />

                <asp:Label ID="Titulo3" Text="Estudiantes con llave de cocina" runat="server" 
                    Font-Size="Large" ForeColor="Black" Font-Bold="True" Font-Names="Arial">
                </asp:Label>
                <br /><br />
                <p>
                    Edita el grupo de cocina al que pertenecen los residentes, haciendo clic en el botón Editar al lado de cada fila
                </p>
                <hr />
        
		        <asp:GridView ID="gvEstudiantes" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered"
                    DataKeyNames="ID_ESTUDIANTE" Visible='<%# ((System.Data.DataTable)gvEstudiantes.DataSource).Rows.Count == 0 ? false : true %>'
                    OnRowEditing="gvEstudiantes_RowEditing"
                    OnRowUpdating="gvEstudiantes_RowUpdating"
                    OnRowCancelingEdit="gvEstudiantes_RowCancelingEdit"
                    OnRowDataBound="gvEstudiantes_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="NOMBRE" HeaderText="ESTUDIANTE" SortExpression="ESTUDIANTE" ReadOnly="true"></asp:BoundField>
                        <asp:TemplateField HeaderText="GRUPO">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddGrupos" DataTextField="GRUPO" runat="server" CssClass="selectpicker form-control"
                                    Width="140px" AutoPostBack="false" Visible="true" OnDataBinding="ddGrupos_DataBinding" Enabled="false">
                                    <asp:ListItem Text="Ninguno" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                    <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                    <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                    <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                    <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" ButtonType="Button" ControlStyle-CssClass="btn btn-warning" 
                                EditText="Editar" UpdateText="Actualizar" CancelText="Cancelar">
                            <ControlStyle CssClass="btn btn-warning"></ControlStyle>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:CommandField>
                    </Columns>
                </asp:GridView>
                <hr />
                <br />
                <br />

                <asp:Label ID="Titulo4" Text="Resumen de los grupos formados y horario" runat="server" 
                    Font-Size="Large" ForeColor="Black" Font-Bold="True" Font-Names="Arial">
                </asp:Label>
                <br /><br />
                <p>
                    Visualiza el horario y las personas agrupadas en sus respectivos grupos
                </p>
                <hr />
                <asp:GridView ID="gvGrupos" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered"
                    Visible="true" AllowSorting="true" DataKeyNames="ID_GRUPO">
                    <Columns>
                        <asp:BoundField DataField="GRUPO" HeaderText="GRUPO"></asp:BoundField>
                        <asp:BoundField DataField="INTEGRANTES" HeaderText="INTEGRANTES" HtmlEncode="false"></asp:BoundField>
                        <asp:BoundField DataField="HORARIO" HeaderText="HORARIO"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script src="Scripts/bootstrap.min.js"></script>
</asp:Content>
