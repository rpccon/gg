﻿<%@ Page Title="Ver Grupos" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Configurar_Grupos_Admin.aspx.cs" Inherits="SICRER_Web_Application.Configurar_Grupos_Admin"  EnableEventValidation="false"
    Culture="Auto" UICulture="Auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="Content/Site.css" rel="stylesheet" />
    <script src="Scripts/site.js" type="text/javascript"></script>

    <div class="col-md-3 space-up">
        <ul class="nav nav-pills nav-stacked well">
            <li ><a href="Consultar_Revisiones.aspx"> Consultar Revisiones</a></li>
            <li class="active"><a href="Configurar_Grupos_Admin.aspx"> Consultar Grupos</a></li>
            <li><a href="Consultar_Estudiantes.aspx">Consultar Estudiantes</a></li>
            <li><a href="Consultar_Activos_Inactivos.aspx">Consultar Activos/Inactivos</a></li>
        </ul>
    </div>
    <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
     	<ContentTemplate>
            <div class="col-md-9 space-up">
                <div>
                    <asp:Panel ID="panelMsjExito" runat="server" CssClass="exito" Visible="false">
                        <asp:Image ID="imgInd" runat="server" ImageUrl="~/img/ok.png"/>
                        <asp:Label ID="lbInd" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </asp:Panel>

                    <asp:Panel ID="panelMsjError" runat="server" CssClass="error" Visible="false">
                        <asp:Image ID="imgIndError" runat="server" ImageUrl="~/img/error_icon.png"/>
                        <asp:Label ID="lbIndError" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </asp:Panel>

                    <asp:Panel ID="panelMsjAdv" runat="server" CssClass="alerta" Visible="false">
                        <asp:Image ID="imgIndAdv" runat="server" ImageUrl="~/img/warning_icon.png"/>
                        <asp:Label ID="lbIndAdv" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </asp:Panel>    
                </div>
                <div class="form-inline">
                    <asp:DropDownList ID="ddResidencia" runat="server" DataTextField="NUMERO_RESIDENCIA" CssClass="selectpicker form-control" Width="150px" AutoPostBack="false">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddAla" runat="server" CssClass="selectpicker form-control" Width="150px" AutoPostBack="false">
                        <asp:ListItem Text="A" Value="A"></asp:ListItem>
                        <asp:ListItem Text="B" Value="B"></asp:ListItem>
                        <asp:ListItem Text="Ala" Value="Ala"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:LinkButton ID="linkBuscar" runat="server" CausesValidation="false" Text="<span class='glyphicon glyphicon-search'></span> Mostrar Estudiantes " CssClass="btn btn-primary" OnClick="linkBuscar_Click"></asp:LinkButton>
                </div>

                <br /><br />
                <asp:Label ID="tipoHorariotxt" Text="" runat="server" 
                    Font-Size="Medium" ForeColor="Black" Font-Bold="True" Font-Names="Arial">
                </asp:Label>
        
                <br /> <br />
                <asp:GridView ID="gvEstudiantes" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered"
                    DataKeyNames="ID_ESTUDIANTE" Visible='<%# ((System.Data.DataTable)gvEstudiantes.DataSource).Rows.Count == 0 ? false : true %>'
                    >
                    <Columns>
                        <asp:BoundField DataField="NOMBRE" HeaderText="ESTUDIANTE" SortExpression="ESTUDIANTE" ReadOnly="true"></asp:BoundField>
                        <asp:BoundField DataField="GRUPO" HeaderText="GRUPO" SortExpression="GRUPO" ReadOnly="true"></asp:BoundField>
                    </Columns>
                </asp:GridView>
        
                <br /> <br />
        
                <asp:GridView ID="gvGrupos" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered"
                    Visible="true" AllowSorting="true" DataKeyNames="ID_GRUPO">
                    <Columns>
                        <asp:BoundField DataField="GRUPO" HeaderText="GRUPO"></asp:BoundField>
                        <asp:BoundField DataField="INTEGRANTES" HeaderText="INTEGRANTES" HtmlEncode="false"></asp:BoundField>
                        <asp:BoundField DataField="HORARIO" HeaderText="HORARIO"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
        </ContentTemplate>
   </asp:UpdatePanel>
    <script src="Scripts/bootstrap.min.js"></script>
</asp:Content>
