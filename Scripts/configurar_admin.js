﻿function volver() {
    history.back();
}
var hiddenInput;
$(document).ready(function () {
    var options = {
        trigger: 'hover focus'
    }
    $(function () {
        $('[data-toggle="popover"]').popover(options);
        debugger;
    })

    $(function () {
        $('[data-toggle="tooltip"]').tooltip({ fade: 250 });
        debugger;
    })
    hiddenInput = document.getElementById('<%=HiddenField1.ClientID %>');
})


function cargandoPagina() {
    var valor = hiddenInput.value;

    if (valor == 1) {

        selectorAdministrador(1);
        esconderColumnasAdministrador();
        habilitarRequest();

    }
    else if (valor == 2) {
        selectorRevisor(1);
        esconderColumnasRevisor();
        habilitarRequest();

    }
    else if (valor == 3) {
        selectorCoordinador(1);
        esconderColumnasCoordinador();
        habilitarRequest();
    }

}


$(document).ready(function () {


    cargandoPagina();
    inhabilitarRequest();
    $("#tabla").css("display", "block");
    debugger;
    $('#MainContent_gvAdministradores tr').each(function () {

        $(this).find('th').each(function () {
            var columna = $(this).index();
            if (columna == 0) {
                $(this).css("display", "none");
            }
        });
        $(this).find('td').each(function () {
            var columna = $(this).index();
            if (columna == 0) {
                $(this).css("display", "none");
            }

        });
    });



});

function limpiarCampos() {
    document.getElementById('<%=tbNombre1.ClientID %>').value = "";
    document.getElementById('<%=tbEmail1.ClientID %>').value = "";
    document.getElementById('<%=tbCarne1.ClientID %>').value = "";
    document.getElementById('<%=tbTelefono1.ClientID %>').value = "";
    document.getElementById('<%=tbUsuario1.ClientID %>').value = "";
    document.getElementById('<%=tbContrasena1.ClientID %>').value = "";
    document.getElementById('<%=tbRepetirContrasena1.ClientID %>').value = "";

    document.getElementById('<%=RequiredFieldValidator1.ClientID %>').style.display = "none";
    document.getElementById('<%=RequiredFieldValidator2.ClientID %>').style.display = "none";
    document.getElementById('<%=RequiredFieldValidator3.ClientID %>').style.display = "none";
    document.getElementById('<%=RequiredFieldValidator4.ClientID %>').style.display = "none";
    document.getElementById('<%=RequiredFieldValidator5.ClientID %>').style.display = "none";
    document.getElementById('<%=RequiredFieldValidator6.ClientID %>').style.display = "none";





}

function inhabilitarRequest() {
    document.getElementById('<%=RequiredFieldValidator1.ClientID %>').enabled = false;
    document.getElementById('<%=RequiredFieldValidator2.ClientID %>').enabled = false;
    document.getElementById('<%=RequiredFieldValidator3.ClientID %>').enabled = false;
    document.getElementById('<%=RequiredFieldValidator4.ClientID %>').enabled = false;
    document.getElementById('<%=RequiredFieldValidator5.ClientID %>').enabled = false;
    document.getElementById('<%=RequiredFieldValidator6.ClientID %>').enabled = false;
}

function habilitarRequest() {
    if (hiddenInput.value == 1) {
        document.getElementById('<%=RequiredFieldValidator1.ClientID %>').enabled = true;

        document.getElementById('<%=RequiredFieldValidator3.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator4.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator5.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator6.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator2.ClientID %>').enabled = false;
    }
    else if (hiddenInput.value == 2) {
        document.getElementById('<%=RequiredFieldValidator1.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator3.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator4.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator5.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator6.ClientID %>').enabled = false;
        document.getElementById('<%=RequiredFieldValidator2.ClientID %>').enabled = false;

    }
    else if (hiddenInput.value == 3) {
        document.getElementById('<%=RequiredFieldValidator1.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator2.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator3.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator4.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator5.ClientID %>').enabled = true;
        document.getElementById('<%=RequiredFieldValidator6.ClientID %>').enabled = false;
        debugger;
    }
    debugger;
}

function ocultarFormulario() {

    $("#formulario").css("display", "none");
    document.getElementById('<%=RequiredFieldValidator1.ClientID %>').enabled = false;
    document.getElementById('<%=RequiredFieldValidator2.ClientID %>').enabled = false;
    document.getElementById('<%=RequiredFieldValidator3.ClientID %>').enabled = false;
    document.getElementById('<%=RequiredFieldValidator4.ClientID %>').enabled = false;
    document.getElementById('<%=RequiredFieldValidator5.ClientID %>').enabled = false;
    document.getElementById('<%=RequiredFieldValidator6.ClientID %>').enabled = false;


}
function mostrarFormulario() {

    $("#formulario").css("display", "block");
    debugger;
    habilitarRequest();


}
function SetDataField(data) {
    var hiddenInput = document.getElementById('<%=HiddenField1.ClientID %>');
    hiddenInput.value = data;


}
function selectorRevisor(indice) {
    SetDataField(2);
    habilitarRequest();

    if (indice == 0) {
        limpiarCampos();

        document.getElementById('<%=Button1.ClientID %>').className = "btn btn-primary";
        document.getElementById('<%=Button1.ClientID %>').text = "Registrar";

    }
    $("#tipoUsuario").text("Registro de Revisor");

    $("#divTelefono").css("display", "none");
    $("#divEmail").css("display", "none");
    $("#divCarne").css("display", "none");
    document.getElementById('<%=tbNombre1.ClientID %>').disabled = false;
    document.getElementById('<%=buscar.ClientID %>').placeholder = "Buscar Revisor";

    $("#tipoRev").addClass("active");
    $("#tipoAdmin").removeClass("active");
    $("#tipoCor").removeClass("active");




    esconderColumnasRevisor();

}
function selectorAdministrador(indice) {
    SetDataField(1);
    habilitarRequest();

    debugger;
    if (indice == 0) {
        limpiarCampos();
        debugger;
        document.getElementById('<%=Button1.ClientID %>').className = "btn btn-primary";
        document.getElementById('<%=Button1.ClientID %>').text = "Registrar";
    }
    $("#tipoUsuario").text("Registro de Administrador");

    $("#divTelefono").css("display", "block");
    $("#divEmail").css("display", "block");
    $("#divCarne").css("display", "none");
    document.getElementById('<%=tbNombre1.ClientID %>').disabled = false;
    document.getElementById('<%=buscar.ClientID %>').placeholder = "Buscar Administrador";

    $("#tipoRev").removeClass("active");
    $("#tipoAdmin").addClass("active");
    $("#tipoCor").removeClass("active");




    esconderColumnasAdministrador();


}
function selectorCoordinador(indice) {
    SetDataField(3);
    habilitarRequest();

    if (indice == 0) {

        limpiarCampos();

        document.getElementById('<%=Button1.ClientID %>').className = "btn btn-primary";
        document.getElementById('<%=Button1.ClientID %>').text = "Registrar";


    }
    $("#tipoUsuario").text("Registro de Coordinador");
    $("#divTelefono").css("display", "none");
    $("#divEmail").css("display", "none");
    $("#divCarne").css("display", "block");

    $('<%=tbNombre1.ClientID %>').attr("placeholder", "Estudiante");
    $("#tipoRev").removeClass("active");
    $("#tipoAdmin").removeClass("active");

    $("#tipoCor").addClass("active");

    document.getElementById('<%=buscar.ClientID %>').placeholder = "Buscar Coordinador";
    document.getElementById('<%=tbNombre1.ClientID %>').disabled = true;

    esconderColumnasCoordinador();



}

function esconderColumnasCoordinador() {
    var gridrows = document.getElementById('MainContent_gvAdministradores').rows;

    for (i = 0; i < gridrows.length; i++) {
        gridrows[i].cells[1].style.display = "none";
        gridrows[i].cells[2].style.display = "none";
        gridrows[i].cells[3].style.display = "none";
        gridrows[i].cells[4].style.display = "none";
    }

    $('#MainContent_gvAdministradores').each(function () {
        $(this).find('tr').each(function () {
            var admin = $(this).find("select option:selected").text();

            if (admin == "Administrador" || admin == "Revisor") {
                $(this).css("display", "none");
            }
            else if (admin == "Coordinador") {
                $(this).css("display", "");
            }

        });
    });
    return false;

}
function esconderColumnasAdministrador() {
    var gridrows = document.getElementById('MainContent_gvAdministradores').rows;

    for (i = 0; i < gridrows.length; i++) {
        gridrows[i].cells[1].style.display = "";
        gridrows[i].cells[2].style.display = "";
        gridrows[i].cells[3].style.display = "";
        gridrows[i].cells[4].style.display = "none";

    }


    $('#MainContent_gvAdministradores').each(function () {
        $(this).find('tr').each(function () {
            var admin = $(this).find("select option:selected").text();

            if (admin == "Revisor" || admin == "Coordinador") {
                $(this).css("display", "none");
            }
            else if (admin == "Administrador") {
                $(this).css("display", "");
            }

        });
    });
    return false;
}

function esconderColumnasRevisor() {
    var gridrows = document.getElementById('MainContent_gvAdministradores').rows;
    debugger;
    for (i = 0; i < gridrows.length; i++) {
        gridrows[i].cells[1].style.display = "none";
        gridrows[i].cells[2].style.display = "none";
        gridrows[i].cells[3].style.display = "none";
        if (gridrows[i].cells[4].style.display == "none") {
            gridrows[i].cells[4].style.display = "";

        }
    }

    $('#MainContent_gvAdministradores').each(function () {
        $(this).find('tr').each(function () {
            var admin = $(this).find("select option:selected").text();

            if (admin == "Administrador" || admin == "Coordinador") {
                $(this).css("display", "none");
            }
            else if (admin == "Revisor") {
                $(this).css("display", "");
            }

        });
    });
    return false;
}